from sklearn.metrics import roc_auc_score


def gini_coefficient(estimator, X, y):
    """
    Calculate gini coefficient
    :param estimator: sklearn estimator
    :param X: pandas DataFrame with data
    :param y: target variable
    :return: flaot - Gini coefficient
    """
    y_pred = estimator.predict_proba(X)[:, 1]
    return 2 * roc_auc_score(y, y_pred) - 1
