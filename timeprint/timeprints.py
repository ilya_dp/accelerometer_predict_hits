import os
import pandas as pd
import warnings


warnings.simplefilter('ignore')


def generate_data(X, window_size=2000, window_shift=200, group_col=None, time_col=None):
    min_time = min(X[time_col])
    max_time = max(X[time_col])

    event_id = 1
    Xt = []
    yt = []
    while True:
        print('Event number {}. Min Time {}'.format(event_id, min_time))
        mask = X[(X[time_col] >= min_time) & (X[time_col] < min_time + window_size)]

        mask['event_id'] = event_id
        min_time += window_shift

        # chek if data exists
        if len(mask) == 0:
            continue

        event_id += 1

        # add data to main objects
        len_group = len(mask)
        hits = sum((mask[group_col] > 0).astype(int))
        prc_hits = hits/len_group

        # drop odd columns
        mask = mask[['event_id', 'time', 'ax', 'ay', 'az', 'gx', 'gy', 'gz']]

        yt.append((event_id, hits, len_group, prc_hits))
        Xt.append(mask)
        del mask

        if min_time > max_time:
            return pd.concat(Xt), pd.DataFrame(yt, columns=['event_id', 'hits', 'total', 'prc_hits'])


if __name__ == '__main__':
    # read data
    X = pd.read_csv('../data/itsuba_processed.csv', sep=',')
    # split data on subsamples
    Xt, yt = generate_data(X, window_size=2000, window_shift=200, group_col='id', time_col='time')
