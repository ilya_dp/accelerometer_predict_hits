import pandas as pd


def assign_ids_to_events(filename, sep=',', hit_column='range', start_hit='begin',
                         end_hit='end', id_column='id', result_filename=None):
    """
    Assign unique ID for events and write result to file.
        Positive number for hits, negative - for other
    :param filename: filename
    :param sep: columns delimiter
    :param hit_column: column where info about hits are stored
    :param start_hit: value that signalize that hit started
    :param end_hit: value that signalize that hit finished
    :param id_column: the name of id column
    :param result_filename: the name of result file
    """
    # read the data
    X = pd.read_csv(filename, sep=sep)
    # create column to assign IDs
    X[id_column] = 0
    good_id, bad_id, is_hit = -1, 1, False

    # assign IDs
    for index, row in X.iterrows():
        hit_comment = row[hit_column]

        if hit_comment == start_hit:
            is_hit = True
            bad_id += 1

        if hit_comment == end_hit:
            is_hit = False
            good_id -= 1
        # assign ID to current row
        X[id_column][index] = bad_id if is_hit else good_id
    # write data to csv file
    X.to_csv(result_filename, index=False, header=True, sep=sep)


if __name__ == '__main__':
    assign_ids_to_events(
        'data/itsuba.csv',
        sep=',',
        hit_column='range',
        start_hit='begin',
        end_hit='end',
        id_column='id',
        result_filename='data/itsuba_processed.csv'
    )