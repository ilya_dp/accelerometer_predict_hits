from sklearn.ensemble import GradientBoostingClassifier
from sklearn.cross_validation import train_test_split
from feature_engineering import feature_extraction
from sklearn.metrics import confusion_matrix
from evaluation import gini_coefficient
import warnings
import pandas as pd

warnings.simplefilter('ignore')


if __name__ == '__main__':
    # read and prepare data
    filename = 'data/itsuba_processed.csv'
    raw_data = pd.read_csv(filename, sep=',', usecols=['id', 'ax', 'ay', 'az', 'gx', 'gy', 'gz'])
    X, y = feature_extraction(raw_data, id_column='id', aggregate_functions=['mean', 'max', 'min'])
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=0)

    # create estimator
    estimator = GradientBoostingClassifier(n_estimators=100, max_depth=2, random_state=0)
    estimator.fit(X_train, y_train)

    # calculate gini coefficient
    print(gini_coefficient(estimator, X_train, y_train), gini_coefficient(estimator, X_test, y_test))

    # print confusion matrices for train and test set
    print(confusion_matrix(y_train, estimator.predict(X_train)))
    print(confusion_matrix(y_test, estimator.predict(X_test)))
