def feature_extraction(X, id_column, aggregate_functions=None):
    """
    Extract features from raw data
    :param X: pandas DataFrame
    :param id_column: id column
    :param aggregate_functions: list of functions to use (min, max, mean)
    :return: extracted features
    """
    X_result = X.groupby([id_column]).agg(aggregate_functions)
    y = (X_result.index > 0).astype(float)
    return X_result, y


